﻿using NUnit.Framework;

namespace SeleniumConsoleApp
{
    public class TextBoxTests : HtmlElementUtilities
    {
        [Test]
        public void TextBox_IdSelection()
        {
            OpenUrl(GetHomePageUrl());
            WriteToTextBoxWithId("textBox", "Hello World");
            var textBoxContent = ReadFromtextBoxWithId("textBox");
            Assert.AreEqual("Hello World", textBoxContent);
        }

        [Test]
        public void TextBox_ClassSelection()
        {
            OpenUrl(GetHomePageUrl());
            WriteToTextBoxWithClassName("textBoxClass", "Hello World");
            var textBoxContent = ReadFromtextBoxWithClass("textBoxClass");
            Assert.AreEqual("Hello World", textBoxContent);

        }
        [Test]
        public void TextBox_NameSelection()
        {
            OpenUrl(GetHomePageUrl());
            WriteToTextBoxWithName("textBoxName", "Hello World");
            var textBoxContent = ReadFromtextBoxWithName("textBoxName");
            Assert.AreEqual("Hello World", textBoxContent);
        }




    }
}
