﻿using NUnit.Framework;

namespace SeleniumConsoleApp
{
    class CheckBox : HtmlElementUtilities
    {
        [Test]
        public void CheckBox_SelectSingleOption()
        {
            OpenUrl(GetHomePageUrl());
            SelectCheckBoxWithId("checkBoxId");
            bool isChecked = IsCheckBoxChecked("checkBoxId");
            Assert.AreEqual(isChecked, true);
        }


    }
}
