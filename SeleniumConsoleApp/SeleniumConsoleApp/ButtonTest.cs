﻿using NUnit.Framework;

namespace SeleniumConsoleApp
{
    public class ButtonTest : HtmlElementUtilities
    {
        [Test]
        public void Button_clickWithId()
        {
            OpenUrl(GetHomePageUrl());
            ClickButtonWithId("buttonId");
        }
        [Test]
        public void Button_clickWithName()
        {
            OpenUrl(GetHomePageUrl());
            ClickButtonWithName("buttonName");
        }
        [Test]
        public void Button_clickWithClass()
        {
            OpenUrl(GetHomePageUrl());
            ClickButtonWithClass("buttonClass");
        }


    }
}
