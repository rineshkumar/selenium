﻿using NUnit.Framework;

namespace SeleniumConsoleApp
{
    public class SelectOptionTests : HtmlElementUtilities
    {
        [Test]
        public void Select_NameTest()
        {
            OpenUrl(GetHomePageUrl());
            SelectAnOptionBySelectNameAndOptionValue("cars", "volvo");
            string value = GetOptionValueUsingSelectNameAndOptionValue("cars");
            Assert.AreEqual("volvo", value);
        }
        [Test]
        public void Select_IdTest()
        {
            OpenUrl(GetHomePageUrl());
            SelectAnOptionBySelectIdAndOptionValue("vehicles", "volvo");
            string value = GetOptionValueUsingSelectIdAndOptionValue("vehicles");
            Assert.AreEqual("volvo", value);
        }


    }
}
