﻿using NUnit.Framework;
using OpenQA.Selenium;

namespace SeleniumConsoleApp
{

    class Program : HtmlElementUtilities
    {

        static void Main(string[] args)
        {
            //OpenGoogleBrowser();
            //Console.ReadLine();
        }

        [Test]
        public void ExecuteTest()
        {
            System.Console.WriteLine("Executing test ");
            //Open google chrome 
            OpenUrl("http://www.google.com");
            IWebElement element = driver.FindElement(By.Id("lst-ib"));
            //Perform ops 
            element.SendKeys("Hello World");
        }
    }
}
