﻿using System;
using NUnit.Framework;

namespace SeleniumConsoleApp
{
    public class TextAreaTest : HtmlElementUtilities
    {
        [Test]
        public void TextArea_IdSelection()
        {
            OpenUrl(GetHomePageUrl());
            WriteToTextAreaWithId("textArea", "Hello World");
            string content = ReadFromTextAreaWithId("textArea");
            Assert.AreEqual(content, "Hello World");
        }
        [Test]
        public void TextArea_ClassSelection()
        {
            OpenUrl(GetHomePageUrl());
            WriteToTextAreaWithClassName("textAreaClass", "Hello World");
            string textBoxContent = ReadFromtextAreaWithClass("textAreaClass");
            Assert.AreEqual("Hello World", textBoxContent);

        }
        [Test]
        public void TextArea_NameSelection()
        {
            OpenUrl(GetHomePageUrl());
            WriteToTextAreaWithName("textAreaName", "Hello World");
            var textBoxContent = ReadFromtextAreaWithName("textAreaName");
            Assert.AreEqual("Hello World", textBoxContent);
        }

            
    }
}
