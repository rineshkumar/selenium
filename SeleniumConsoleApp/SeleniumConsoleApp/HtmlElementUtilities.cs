﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System.Configuration;

namespace SeleniumConsoleApp
{
    public class HtmlElementUtilities
    {
        public IWebDriver driver;
        #region Utilities
        public void OpenUrl(string url)
        {
            driver.Navigate().GoToUrl(url);
        }
        public string GetHomePageUrl()
        {
            return ConfigurationManager.AppSettings["ContextPath"];
        }
        #endregion
        #region Test Methods 
        [SetUp]
        public void Initialize()
        {
            driver = new ChromeDriver();
            System.Console.WriteLine("Running setup ");
        }
        [TearDown]
        public void CleanUp()
        {
            System.Console.WriteLine("Cleaning up ");

            driver.Close();
        }
        #endregion
        #region Core Methods 
        public IWebElement GetElementUsingId(string id)
        {
            return driver.FindElement(By.Id(id));
        }
        public IWebElement GetElementUsingClass(string className)
        {
            return driver.FindElement(By.ClassName(className));

        }
        private IWebElement GetElementUsingName(string textBoxName)
        {
            return driver.FindElement(By.Name(textBoxName));
        }
        private void EnterText(string content, IWebElement element)
        {
            element.SendKeys(content);
        }
        #endregion
        #region Working with Text Box 

        public void WriteToTextBoxWithId(string id, string content)
        {
            var textElement = GetElementUsingId(id);
            EnterText(content, textElement);
        }
        public string ReadFromtextBoxWithId(string id)
        {
            var textElement = GetElementUsingId(id);
            return textElement.GetAttribute("value");
        }
        public string ReadFromtextBoxWithClass(string className)
        {
            var textElement = GetElementUsingClass(className);
            return textElement.GetAttribute("value");

        }
        public void WriteToTextBoxWithClassName(string className, string content)
        {
            var textElement = GetElementUsingClass(className);
            EnterText(content, textElement);
        }
        public string ReadFromtextBoxWithName(string textBoxName)
        {
            var textElement = GetElementUsingName(textBoxName);
            return textElement.GetAttribute("value");
        }
        public void WriteToTextBoxWithName(string textBoxName, string content)
        {
            var textElement = GetElementUsingName(textBoxName);
            EnterText(content, textElement);
        }
        #endregion
        #region working with text area 
        public void WriteToTextAreaWithId(string id, string content)
        {
            var textAreaElement = GetElementUsingId(id);
            EnterText(content, textAreaElement);

        }
        public string ReadFromTextAreaWithId(string id)
        {
            var textAreaElement = GetElementUsingId(id);
            return textAreaElement.GetAttribute("value");
        }
        public string ReadFromtextAreaWithClass(string className)
        {
            var textAreaElement = GetElementUsingClass(className);
            return textAreaElement.GetAttribute("value");
        }

        public void WriteToTextAreaWithClassName(string className, string content)
        {
            var textAreaElement = GetElementUsingClass(className);
            EnterText(content, textAreaElement);
        }
        public string ReadFromtextAreaWithName(string textAreaName)
        {
            var textAreaElement = GetElementUsingName(textAreaName);
            return textAreaElement.GetAttribute("value");
        }

        public void WriteToTextAreaWithName(string textAreaName, string content)
        {
            var textElement = GetElementUsingName(textAreaName);
            EnterText(content, textElement);

        }

        #endregion
        #region Working with selection option 
        public void SelectAnOptionBySelectNameAndOptionValue(string selectName, string optionValue)
        {
            var element = GetElementUsingName(selectName);
            SelectAnOptionUsingValue(optionValue, element);

        }

        private static void SelectAnOptionUsingValue(string optionValue, IWebElement element)
        {
            SelectElement selectElement = new SelectElement(element);
            selectElement.SelectByValue(optionValue);
        }

        public string GetOptionValueUsingSelectNameAndOptionValue(string selectName)
        {
            var element = GetElementUsingName(selectName);
            return GetSelectedOptionAttributeValue(element);

        }

        private static string GetSelectedOptionAttributeValue(IWebElement element)
        {
            SelectElement selectElement = new SelectElement(element);
            return selectElement.SelectedOption.GetAttribute("value");
        }

        public string GetOptionValueUsingSelectIdAndOptionValue(string selectId)
        {
            var element = GetElementUsingId(selectId);
            return GetSelectedOptionAttributeValue(element);
        }

        public void SelectAnOptionBySelectIdAndOptionValue(string selectId, string optionValue)
        {
            var element = GetElementUsingId(selectId);
            SelectAnOptionUsingValue(optionValue, element);
        }
        #endregion
        #region Button related methods 
        public void ClickButtonWithId(string buttonId)
        {
            GetElementUsingId(buttonId).Click();
        }
        public void ClickButtonWithName(string buttonName)
        {
            GetElementUsingName(buttonName).Click();
        }
        public void ClickButtonWithClass(string className)
        {
            GetElementUsingClass(className).Click();
        }
        #endregion
        #region Working with data list 
        public void SelectFromDataListWithIdAndValue(string dataListId, string optionValueSubString)
        {
            GetElementUsingId(dataListId).Clear();
            GetElementUsingId(dataListId).SendKeys(optionValueSubString);
        }
        #endregion
        #region Check Box
        public void SelectCheckBoxWithId(string checkBoxId)
        {
            GetElementUsingId(checkBoxId).Click();
        }
        public bool IsCheckBoxChecked(string checkBoxId)
        {
            return GetElementUsingId(checkBoxId).Selected;
        }
        #endregion
    }
}
